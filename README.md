### A Minimalist Serial UART Driver for the Beaglebone Black

    Copyright (C) 2020, Alex Rhodes <https://www.alexrhodes.io>


    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or (at
    your option) any later version.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    <https://www.gnu.org/licenses/gpl-3.0.html>
 

This project contains a minimalist serial UART driver and device tree for the Beaglebone Black.


**This project is described in detail in tutorial form in a [blog post on my website.](https://alexrhodes.io/blog/post/22/)**